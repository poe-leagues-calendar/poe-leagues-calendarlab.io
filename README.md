# Path of Exile Leagues Calendar

## Features
* This small page was used to check when a Path of Exile league starts and ends.
* It's using [fullcaldendar.io](https://fullcalendar.io) to show a simple calendar.
* Past and future leagues aren't shown because the PoE API does not show them, just the current league.